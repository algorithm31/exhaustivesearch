/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thanya.exhaustivesearch;

/**
 *
 * @author Thanya
 */
public class ExhaustiveSearch {

    static void printSeperatearr(int[] arr, int seperatesize) {
        int[] tem = new int[seperatesize];
        for (int i = 0; i < seperatesize; i++) {
            tem[i] = arr[0];
            arr = remove(arr, 0);
        }
        printarr(tem);
        printarr(arr);
    }

    static int[] swap(int[] arr, int a, int b) {
        int tem = arr[a];
        arr[a] = arr[b];
        arr[b] = tem;
        return arr;
    }

    static void permute(int[] arr, int b, int seperatesize) {
        for (int i = b; i < arr.length; i++) {
            arr = swap(arr, i, b);
            permute(arr, b + 1, seperatesize);
            arr = swap(arr, b, i);
        }
        if (b == arr.length - 1) {
            printSeperatearr(arr, seperatesize);
            System.out.println("");
        }
    }

    static int[] remove(int[] arr, int index) {
        int[] temarr = new int[arr.length - 1];
        if (index == arr.length - 1) {
            for (int i = 0; i < temarr.length; i++) {
                temarr[i] = arr[i];
            }
        } else {
            int count = 0;
            for (int i = 0; i < temarr.length; i++) {
                if (i == index) {
                    count++;
                    temarr[i] = arr[i + count];
                } else {
                    temarr[i] = arr[i + count];
                }
            }
        }
        return arr;
    }

    static int[] removecontain(int[] arr, int value) {
        for (int i = 0; i < arr.length; i++) {
            if (arr[i] == value) {
                arr = remove(arr, i);
                return arr;
            }
        }
        return arr;
    }

    static void printarr(int[] arr) {
        if (arr.length > 0) {
            for (int i = 0; i < arr.length; i++) {
                if (arr.length == 1) {
                    System.out.print("[" + arr[i] + "]");
                } else if (i == 0) {
                    System.out.print("[" + arr[i] + ", ");
                } else if (i == arr.length - 1) {
                    System.out.print(arr[i] + "]");
                } else {
                    System.out.print(arr[i] + ", ");
            }
        }
    } else {
            System.out.print("[]");
        }
    }
    
    static int[] add(int[] arr, int value) {
        int[] tem = new int[arr.length + 1];
        for (int i = 0; i < tem.length; i++) {
            if(i + 1 != tem.length) {
                tem[i] = arr[i];
            } else {
                tem[i] = value;
            }
        }
        return tem;
    }
    
    static boolean getSeperateSubset(int arr[], int n, int sum, int[] collection) {
        if (sum == 0) {
            System.out.print("Can be divided into ");
            printarr(collection);
            System.out.print(" and ");
            for (int i = 0; i < collection.length; i++) {
                arr = removecontain(arr, collection[i]);
            }
            printarr(arr);
            System.out.println("");
            return true;
        }
        if (n == 0 && sum != 0) {
            return false;
        }
        if (arr[n - 1] > sum) {
           return getSeperateSubset(arr, n - 1, sum, collection);
        }

        return getSeperateSubset(arr, n - 1, sum, collection) || getSeperateSubset(arr, n - 1, sum - arr[n - 1], add(collection, arr[n - 1]));

    }

    static boolean findSeperate(int arr[], int n, int[] collection) {
        int sum = 0;

        for (int i = 0; i < n; i++) {
            sum += arr[i];
        }
        if (sum % 2 != 0) {
            return false;
        }
        return getSeperateSubset(arr, n, sum / 2, collection);
    }

    public static void main(String[] args) {
        int arr[] = {12, 4, 9, 5, 2, 3};
        System.out.println("All possibles in both to array");
        for(int i = 0; i < arr.length + 1; i++) {
            permute(arr, 0, i);
        }
        int[] collection = {};
        int n = arr.length;
        
        if (findSeperate(arr, n, collection) == true) {
            System.out.println("Can be divided into two subsets of equal sum");
        } else {
            System.out.println("Can not be divided into two subsets of equal sum");
        }
    }
}
